﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Konczyna : MonoBehaviour {

    private float katPoczatkowy;
    private float predkoscKatowa;
    private float maksymalnyKat;
    private float minimalnyKat;

    private HingeJoint2D hj;

    // Inicjalizacja
    public void Init(float katPoczatkowy, float predkoscKatowa, float maksymalnyKat, float minimalnyKat)
    {
        this.katPoczatkowy = katPoczatkowy;
        this.predkoscKatowa = predkoscKatowa;
        this.maksymalnyKat = maksymalnyKat;
        this.minimalnyKat = minimalnyKat;

        // Warunki poczatkowe
        hj = gameObject.GetComponent<HingeJoint2D>();
        hj.useMotor = true;
        ustawPredkoscKatowa(hj, this.predkoscKatowa);
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        float obecnyKat = hj.jointAngle;
        // Ruch konczyny tam i spowrotem miedzy katem minimlnym i maksymalnym
        if (obecnyKat >= maksymalnyKat)
            zmienZwrotPredkosciKatowej(hj, -1);
        else if (obecnyKat <= minimalnyKat)
            zmienZwrotPredkosciKatowej(hj, 1);
    }


    private void ustawPredkoscKatowa(HingeJoint2D hj, float predkoscKatowa)
    {
        JointMotor2D m = hj.motor;
        m.motorSpeed = predkoscKatowa;
        hj.motor = m;
    }

    private void zmienZwrotPredkosciKatowej(HingeJoint2D hj, int zwrot)
    {
        // int zwrot:
        // -1  zmiana (lub brak dzialania w przypadku ujemnego zwrotu) zwrotu na ujemny
        //  1  zmiana (lub brak dzialania w przypadku dodatniego zwrotu) zwrotu na dodatni
        JointMotor2D m = hj.motor;
        m.motorSpeed = Mathf.Abs(m.motorSpeed);
        if (zwrot == -1)
            m.motorSpeed *= -1;
        hj.motor = m;
    }
}
