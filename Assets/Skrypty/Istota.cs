﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Istota : MonoBehaviour {

	public void Init(List<float> atrybuty, Color kolor)
    {
        // przyjmuje 16 atrybutow, po 4 na kazdy fragment nogi
        // inicjalizuje wszystkie fragmenty konczyn

        Konczyna[] konczyny = gameObject.GetComponentsInChildren<Konczyna>();  // Pobiera skrypty "Konczyna" z obiektow dzieci

        gameObject.GetComponent<SpriteRenderer>().color = kolor;  // Zmiana koloru tulowia

        for (int i = 0; i < 4; i++)
        {
            konczyny[i].GetComponent<SpriteRenderer>().color = kolor;  // Zmiana koloru nog
            konczyny[i].Init(atrybuty[i * 4], atrybuty[i * 4 + 1], atrybuty[i * 4 + 2], atrybuty[i * 4 + 3]);
        }
    }
}
