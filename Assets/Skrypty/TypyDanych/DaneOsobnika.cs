﻿using System.Collections.Generic;

public class DaneOsobnika
{

    public List<float> geny;
    public float wynik;

    public DaneOsobnika()
    {
        geny = new List<float>();
        wynik = 0;
    }

    public DaneOsobnika(List<float> geny)
    {
        this.geny = geny;
        wynik = 0;
    }

    public DaneOsobnika(DaneOsobnika dane)
    {
        this.geny = new List<float>(dane.geny);
        wynik = dane.wynik;
    }

}
