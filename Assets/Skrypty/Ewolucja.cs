﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Globalization;

// Drobna uwaga nomenklaturowa:
// Gen = Atrybut            gen
// Osobnik = Istota         osobnik
// Pokolenie = Populacja    populacja


public class Ewolucja : MonoBehaviour {
    
    Vector3 pozycja_startowa;  // Miejsce w przestrzeni, w ktorym tworzone sa osobniki
    private int liczebnosc_populacji;  // Ilosc osobnikow w populacji
    private int ilosc_populacji;  // Ilosc populacji generowanych podczas calej ewolucji
    private List<DaneOsobnika> dane_poprzedniej_populacji;
    private float prawdopodobienstwo_krzyzowania;  // P, z jakim osobnik zostanie skrzyzowany z innym
    private float prawdopodobienstwo_mutacji;  // P, z jakim dojdzie do mutacji genu
    private float prawdop_powstania_calkiem_nowego_osobnika;
    private float max_wielkosc_mutacji;  // Maksymalna zmiana, jaka dokona sie w genie podczas mutacji (procentowo)
    private float czas_zycia_populacji;  // W sekundach
    List<GameObject> populacja;
    System.Random random;
    private DaneOsobnika najlepszy_osobnik;
    private int p;  // Licznik pokolen
    
    // boole sterujace updatem
    private bool czy_prowadzic_ewolucje;

    private float gorny_limit_kata;  // Maksymalne wychylenie elementu konczyny
    private float dolny_limit_kata;  // Minimalne...
    private float max_predkosc_katowa;  // Maksymalna wartosc bezwzgledna predkosci obrotu elementu konczyny

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    // Uwaga! Zeby dzialalo, trzeba zakomentowac wnetrze funkcji dopisz_geny_nowej_populacji()

    /*// Debug, prace nad dobranem odpowiednich warunkow poczatkowych (wielokrotne ewolucje z roznymi danymi)
    private int dbg_numer_ewolucji = 0;
    private int dbg_ilosc_ewolucji = 81;
    private float[,] dbg_dane_ewolucji =    { { 10, 50, 150 },        // liczebnosci populacji
                                        { 0.1f, 0.5f, 1 },      // prawd. krzyzowania
                                        { 0.01f, 0.05f, 0.1f }, // prawd. mutacji
                                        { 0.05f, 0.1f, 0.5f } };// max wielkosc mutacji
    
    private int[] dbg_indeksy = { 0, 0, 0, 0 };*/
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    // Use this for initialization
    void Start () {

        // Warunki poczatkowe
        this.pozycja_startowa = new Vector3(0f, 1f, 0f);
        this.liczebnosc_populacji = 100;
        this.ilosc_populacji = 150;
        this.prawdopodobienstwo_krzyzowania = 1f;
        this.prawdopodobienstwo_mutacji = 0.01f;  // 0.05 prawdopodobnie najlepsze
        this.max_wielkosc_mutacji = 0.05f;
        this.prawdop_powstania_calkiem_nowego_osobnika = 1f;
        this.czas_zycia_populacji = 10;

        this.gorny_limit_kata = 180;
        this.dolny_limit_kata = -180;
        this.max_predkosc_katowa = 500;

        // Inicjalizacja
        this.populacja = new List<GameObject>();
        this.dane_poprzedniej_populacji = new List<DaneOsobnika>();
        this.najlepszy_osobnik = new DaneOsobnika();
        this.p = 0;
        this.czy_prowadzic_ewolucje = true;
        random = new System.Random();

        // Ustawienia silnika
        //Physics2D.positionIterations = 20;  // Czestotliwosc obliczania wartosci fizycznych (defaultowo ok. 7)
        //Physics2D.velocityIterations = 20;

        UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);

        Time.timeScale = 1;  // (0-100)


        // Usuwanie danych z poprzedniej ewolucji jesli istnieja
        dbg_usun_plik("wyniki_srednie.txt");
        dbg_usun_plik("wyniki.txt");
        dbg_usun_plik("atrybuty.txt");
        dbg_usun_plik("najlepszy_osobnik.txt");

        // Floaty beda wyswietlane i zapisywane z przecinkiem zamiast kropki (dla latwiejego przenoszenia do Excela)
        System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("pl-PL");
    }

    // Update is called once per frame
    void Update () {
        
        if (this.czy_prowadzic_ewolucje)
        {
            this.czy_prowadzic_ewolucje = false;
            StartCoroutine(zegarPokolen());     // Ustawienie timera, ktory odblokuje ewolucje
                                                // po  czasie zycia populacji

            float pozostaly_czas = (ilosc_populacji - p) * czas_zycia_populacji / 60f / Time.timeScale;  // w minutach
            print("Populacja " + this.p + "/" + ilosc_populacji + "\tpozostalo " + pozostaly_czas.ToString("0") + " min.");

            List<DaneOsobnika> dane_nowej_populacji = new List<DaneOsobnika>();

            // Generowanie pierwszego, calkiem losowego pokolenia
            if (p == 0)
            {
                /*List<DaneOsobnika> naj = new List<DaneOsobnika>();
                float[] najgeny_tab = { 20.54276f, -209.0702f, 80.66785f, -71.09915f, -15.11152f, -250f, -54.07189f, 18.515f, 0.8188275f, -105.7496f, -53.55348f, 32.66608f, 75.43514f, 25.02826f, -5.503254f, -50.33695f  };
                List<float> najgeny = new List<float>(najgeny_tab);
                for (int i = 0; i < liczebnosc_populacji; i++)
                    naj.Add(new DaneOsobnika(najgeny));

                dane_nowej_populacji = naj;*/

                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                /*{   // Praca nad ustaleniem warunkow poczatkowych
                    liczebnosc_populacji = (int)dbg_dane_ewolucji[0, dbg_indeksy[0]];
                    prawdopodobienstwo_krzyzowania = dbg_dane_ewolucji[1, dbg_indeksy[1]];
                    prawdopodobienstwo_mutacji = dbg_dane_ewolucji[2, dbg_indeksy[2]];
                    max_wielkosc_mutacji = dbg_dane_ewolucji[3, dbg_indeksy[3]];
                }*/
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                dane_nowej_populacji = generuj_losowe_geny();

            }
            
            else if (p > 0 && this.p < this.ilosc_populacji)  // Kolejne pokolenia
            {
                // Ustalanie wynikow poszczegolnych osobnikow
                // na podstawie pozycji x, do ktorej doszly
                for (int i = 0; i < this.liczebnosc_populacji; i++)
                    this.dane_poprzedniej_populacji[i].wynik = this.populacja[i].GetComponent<Transform>().position.x;


                dbg_dopisz_wyniki_i_sredni_wynik(this.dane_poprzedniej_populacji);  // Zapis danych do pliku

                dane_nowej_populacji = generuj_geny_nowej_populacji(dane_poprzedniej_populacji);

                // Usuwanie starej populacji
                for (int i = 0; i < this.liczebnosc_populacji; i++)
                    Destroy(this.populacja[i]);

            }

            else if (this.p == this.ilosc_populacji)  // Zakonczenie ewolucji
            {
                StopCoroutine(zegarPokolen());
                Time.timeScale = 0.0f;
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                //print("Koniec ewolucji " + dbg_numer_ewolucji + "/" + dbg_ilosc_ewolucji);
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                print("Koniec ewolucji");

                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                /*{
                    dbg_numer_ewolucji++;
                    if (dbg_numer_ewolucji == dbg_ilosc_ewolucji)
                    {
                        Application.Quit();
                    }
                    if (dbg_numer_ewolucji % 2  == 0)
                        dbg_indeksy[0]++;
                    else if (dbg_numer_ewolucji % 2 == 1)
                        dbg_indeksy[1]++;
                    else if (dbg_numer_ewolucji % 2 == 2)
                        dbg_indeksy[2]++;
                    else if (dbg_numer_ewolucji % 2 == 3)
                        dbg_indeksy[3]++;
                    p = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        if (dbg_indeksy[i] == 3)
                            dbg_indeksy[i] = 0;
                    }

                    return;
                }*/
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                //Application.Quit();
            }

            if (this.p != this.ilosc_populacji)
            {
                // Tworzenie nowej populacji
                this.populacja = stworz_populacje(dane_nowej_populacji);
                this.dane_poprzedniej_populacji = dane_nowej_populacji;
            }

            this.p++;
        }
    }
    

    private GameObject stworz_osobnika(List<float> geny, Color kolor)
    {
        GameObject osobnik = (GameObject)Instantiate(Resources.Load("Istota"));
        osobnik.transform.position = this.pozycja_startowa;  // Pozycja startowa
        osobnik.GetComponent<Istota>().Init(geny, kolor);  // Atrybuty poczatkowe wszystkich elementow nog

        return osobnik;
    }

    private List<GameObject> stworz_populacje(List<DaneOsobnika> dane_populacji)
    {
        // Instancjonuje 'liczebnosc_populacji' osobnikow w widoku gry w Unity
        // Zwraca tablice stworzonych instancji
        
        dbg_dopisz_geny_populacji(dane_populacji);  // Zapis genow do pliku

        List<GameObject> populacja = new List<GameObject>();
        for (int i = 0; i < this.liczebnosc_populacji; i++)
        {
            GameObject osobnik = stworz_osobnika(dane_populacji[i].geny, losowy_kolor());
            populacja.Add(osobnik);
        }

        return populacja;
    }

    private Color losowy_kolor()
    {
        // Zwraca losowy kolor
        // (nie z pelnej gamy, unika kolorow mocno zblizonych do bialego i czarnego)

        float R, G, B;
        R = UnityEngine.Random.Range(0.2f, 0.8f);
        G = UnityEngine.Random.Range(0.2f, 0.8f);
        B = UnityEngine.Random.Range(0.2f, 0.8f);
        return new Color(R, G, B);
    }

    private List<DaneOsobnika> generuj_losowe_geny()
    {
        List<DaneOsobnika> dane_populacji = new List<DaneOsobnika>();

        for (int i = 0; i < this.liczebnosc_populacji; i++)  // Dla kazdej istoty
        {
            List<float> geny_osobnika = new List<float>();

            for (int j = 0; j < 4; j++)  // Dla kazdego elementu konczyny
            {
                float predkoscKatowa = UnityEngine.Random.Range(-this.max_predkosc_katowa, this.max_predkosc_katowa); 
                float minimalnyKat = UnityEngine.Random.Range(this.dolny_limit_kata, this.gorny_limit_kata);
                float maksymalnyKat = UnityEngine.Random.Range(minimalnyKat, this.gorny_limit_kata);
                float katPoczatkowy = UnityEngine.Random.Range(minimalnyKat, maksymalnyKat);

                geny_osobnika.Add(katPoczatkowy);
                geny_osobnika.Add(predkoscKatowa);
                geny_osobnika.Add(maksymalnyKat);
                geny_osobnika.Add(minimalnyKat);
            }

            DaneOsobnika dane_osobnika = new DaneOsobnika(geny_osobnika);
            dane_populacji.Add(dane_osobnika);
        }

        return dane_populacji;
    }

    private List<DaneOsobnika> generuj_geny_nowej_populacji(List<DaneOsobnika> stare_dane)
    {
        // Operacje genetyczne
        List<DaneOsobnika> dane1, dane2, dane3, dane4, dane5;
        dane1 = new List<DaneOsobnika>(stare_dane);      

        dane2 = ewo_sortowanie(dane1);

        if (dane2[liczebnosc_populacji - 1].wynik > najlepszy_osobnik.wynik)  // Ewentualny update najlepszego osobnika
        {
            dbg_usun_plik("najlepszy_osobnik.txt");
            najlepszy_osobnik = new DaneOsobnika(dane2[liczebnosc_populacji - 1]);
            dbg_zapisz_najlepszego_osobnika();
        }

        dane3 = ewo_wybieranie_przedstawicieli(dane2);
        dane4 = ewo_krzyzowanie(dane3);
        dane5 = ewo_mutowanie(dane4);

        // Nieoptymalne losowanie nowego osobnika (do wymiany)
        float rand = UnityEngine.Random.Range(0f, 1f);
        if (rand < prawdop_powstania_calkiem_nowego_osobnika)
            dane5[dane5.Count - 1].geny = new List<float>(generuj_losowe_geny()[0].geny);
        
        return dane5;
    }



    private List<DaneOsobnika> ewo_sortowanie(List<DaneOsobnika> stare_dane)
    {
        // Sortowanie wynikow w kolejnosci od najnizszego do najwyzszego

        List<DaneOsobnika> nowe_dane = new List<DaneOsobnika>(stare_dane);

        // Sortowanie populacji wg. osiagnietych wynikow
        // http://stackoverflow.com/questions/3309188/how-to-sort-a-listt-by-a-property-in-the-object
        nowe_dane.Sort((x, y) => x.wynik.CompareTo(y.wynik));

        return nowe_dane;
    }
    
    private List<DaneOsobnika> ewo_wybieranie_przedstawicieli(List<DaneOsobnika> stare_dane)
    {
        // Okreslenie przystosowania osobnikow poprzedniej populacji
        // Zasada: przyjmujemy wynik najgorszy jako minimum, a najlepszy jako maksimum
        // Na tej podstawie okreslamy stosunki szans osobnikow na wylosowanie
        // Ostatni osobnik ma zerowe szanse na bycie wylosowanym
        //
        // Sprowadza sie to do tego, ze im osobnik jest lepszy na tle innych,
        // tym ma wieksza szanse na trafienie do nowej populacji

        // Wyliczanie stosunkow przystosowania
        List<float> przystosowanie = new List<float>();
        for (int i = 0; i < this.liczebnosc_populacji; i++)
            przystosowanie.Add(stare_dane[this.liczebnosc_populacji - 1].wynik -
                stare_dane[i].wynik);

        // Normalizacja listy stosunkow przystosowania
        float normalizacja = 0;
        for (int i = 0; i < this.liczebnosc_populacji; i++)
            normalizacja += przystosowanie[i];
        for (int i = 0; i < this.liczebnosc_populacji; i++)
            przystosowanie[i] /= normalizacja;

        // Wyliczanie progow losowania
        List<float> progi = new List<float>();
        for (int i = 0; i < this.liczebnosc_populacji; i++)
        {
            float prog = 0;
            for (int j = 0; j < i; j++)
                prog += przystosowanie[j];
            prog = 1 - prog;
            
            progi.Add(prog);
        }
        progi.Reverse();

        // Losowanie osobnikow do nowej populacji
        List<DaneOsobnika> nowe_dane = new List<DaneOsobnika>();
        for (int i = 0; i < this.liczebnosc_populacji; i++)  // Po kazdej petli do nowej populacji dochodzi 1 osobnik
        {
            float rand = UnityEngine.Random.Range(0.0f, 1.0f);
            for (int j = 0; j < this.liczebnosc_populacji; j++)
            {
                if (rand < progi[j])
                {
                    List<float> stare_geny = stare_dane[j].geny;
                    DaneOsobnika dane_osobnika = new DaneOsobnika(new List<float>(stare_geny));
                    nowe_dane.Add(dane_osobnika);
                    break;
                }
            }
        }

        return nowe_dane;
    }
    
    private List<DaneOsobnika> ewo_krzyzowanie(List<DaneOsobnika> stare_dane)
    {
        List<DaneOsobnika> nowe_dane = new List<DaneOsobnika>(stare_dane);

        for (int i = 0; i < this.liczebnosc_populacji; i++)
        {
            float rand = UnityEngine.Random.Range(0.0f, 1.0f);
            if (rand < this.prawdopodobienstwo_krzyzowania)
            {
                int punkt_krzyzowania = UnityEngine.Random.Range(1, 15);
                int partner = i;
                while (partner == i)  // Unikniecie krzyzowania osobnika z samym soba
                    partner = UnityEngine.Random.Range(0, this.liczebnosc_populacji);
                for (int j = punkt_krzyzowania; j < 16; j++)
                    nowe_dane[i].geny[j] = nowe_dane[partner].geny[j];
            }
        }

        return nowe_dane;
    }
        
    private List<DaneOsobnika> ewo_mutowanie(List<DaneOsobnika> stare_dane)
    {
        List<DaneOsobnika> nowe_dane = new List<DaneOsobnika>(stare_dane);

        for (int i = 0; i < this.liczebnosc_populacji; i++)
        {
            for (int j = 0; j < 16; j++)
            {
                float rand = UnityEngine.Random.Range(0.0f, 1.0f);

                if (rand < this.prawdopodobienstwo_mutacji) // Czy gen bedzie podlegal mutacji?
                {
                    // Tak!
                    float wielkoscMutacji = UnityEngine.Random.Range(-this.max_wielkosc_mutacji, this.max_wielkosc_mutacji);
                    ///float wielkoscMutacji = (float)random.NextDouble() * max_wielkosc_mutacji * dodatnio_lub_ujemnie();

                    float zmutowanyAtrybut;
                    float przed = dane_poprzedniej_populacji[i].geny[j];

                    zmutowanyAtrybut = przed + przed * wielkoscMutacji;
                    
                    // Zabezpieczenie przed wykroczeniem poza ustalone limity genow
                    switch (j % 4)
                    {
                        case 0:  // gen kata poczatkowego
                            if (zmutowanyAtrybut > gorny_limit_kata)
                                zmutowanyAtrybut = gorny_limit_kata;
                            else if (zmutowanyAtrybut < dolny_limit_kata)
                                zmutowanyAtrybut = dolny_limit_kata;
                            break;

                        case 1:  // gen predkosci katowej
                            if (zmutowanyAtrybut > max_predkosc_katowa)
                                zmutowanyAtrybut = max_predkosc_katowa;
                            else if (zmutowanyAtrybut < -max_predkosc_katowa)
                                zmutowanyAtrybut = -max_predkosc_katowa;
                            break;

                        case 2:  // gen maksymalnego kata
                            if (zmutowanyAtrybut > gorny_limit_kata)
                                zmutowanyAtrybut = gorny_limit_kata;
                            else if (zmutowanyAtrybut < dane_poprzedniej_populacji[i].geny[j + 1])
                                zmutowanyAtrybut = dane_poprzedniej_populacji[i].geny[j + 1];  
                                    // ^ Zeby maksymalny kat nie byl mniejszy od minimalnego
                            break;

                        case 3:  // gen minimalnego kata
                            if (zmutowanyAtrybut < dolny_limit_kata)
                                zmutowanyAtrybut = dolny_limit_kata;
                            else if (zmutowanyAtrybut > dane_poprzedniej_populacji[i].geny[j - 1])
                                zmutowanyAtrybut = dane_poprzedniej_populacji[i].geny[j - 1];
                                    // ^ Zeby minimalny kat nie byl wiekszy od maksymalnego
                            break;
                        default:
                            break;
                    }
                    
                    nowe_dane[i].geny[j] = zmutowanyAtrybut;  // Zatwierdzenie mutacji
                }
            }
        }

        return nowe_dane;
    }



    IEnumerator zegarPokolen()
    {
        yield return new WaitForSeconds(this.czas_zycia_populacji);
        this.czy_prowadzic_ewolucje = true;
    }


    private int dodatnio_lub_ujemnie()
    {
        // Zwraca 1 lub -1
        double rand;
        rand = random.NextDouble();
        if (rand < 0.5)
            return 1;
        else
            return -1;
    }

    
    private void dbg_usun_plik(string sciezka)
    {
        if (File.Exists(sciezka))
            File.Delete(sciezka);
    }

    private void dbg_dopisz_wyniki_i_sredni_wynik(List<DaneOsobnika> dane)
    {
        // Debug begin //////////////////////////////////////////////////////////////////////////////////////
        // ZAPIS WYNIKOW I SREDNICH WYNIKOW DO PLIKU
        string dbg_wyniki = string.Empty;
        float dbg_suma = 0f;
        dbg_wyniki += "Populacja " + this.p + Environment.NewLine;
        for (int u = 0; u < this.liczebnosc_populacji; u++)
        {
            dbg_wyniki += dane[u].wynik + Environment.NewLine;
            dbg_suma += dane[u].wynik;
        }
        dbg_wyniki += Environment.NewLine;
        File.AppendAllText("wyniki.txt", dbg_wyniki);

        if (p == 1)
        {
            string naglowek = string.Empty;
            naglowek += "liczebnosc_populacji:\t\t\t\t" + this.liczebnosc_populacji;
            naglowek += Environment.NewLine + "ilosc_populacji:\t\t\t\t" + this.ilosc_populacji;
            naglowek += Environment.NewLine + "prawdopodobienstwo_krzyzowania:\t\t\t" + this.prawdopodobienstwo_krzyzowania;
            naglowek += Environment.NewLine + "prawdopodobienstwo_mutacji:\t\t\t" + this.prawdopodobienstwo_mutacji;
            naglowek += Environment.NewLine + "max_wielkosc_mutacji:\t\t\t\t" + this.max_wielkosc_mutacji;
            naglowek += Environment.NewLine + "prawdop_powstania_calkiem_nowego_osobnika:\t" + this.prawdop_powstania_calkiem_nowego_osobnika;
            naglowek += Environment.NewLine + "czas_zycia_populacji:\t\t\t\t" + this.czas_zycia_populacji;
            naglowek += Environment.NewLine;
            naglowek += Environment.NewLine + "gorny_limit_kata:\t\t\t\t" + this.gorny_limit_kata;
            naglowek += Environment.NewLine + "dolny_limit_kata:\t\t\t\t" + this.dolny_limit_kata;
            naglowek += Environment.NewLine + "max_predkosc_katowa:\t\t\t\t" + this.max_predkosc_katowa;
            naglowek += Environment.NewLine;
            naglowek += Environment.NewLine + "positionIterations:\t\t\t\t" + Physics2D.positionIterations;
            naglowek += Environment.NewLine + "velocityIterations:\t\t\t\t" + Physics2D.velocityIterations;
            naglowek += Environment.NewLine + "timeScale:\t\t\t\t\t" + Time.timeScale;
            naglowek += Environment.NewLine + Environment.NewLine + Environment.NewLine;
            naglowek += "***Srednie wyniki***" + Environment.NewLine;

            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //File.AppendAllText("wyniki_srednie_" + dbg_numer_ewolucji + ".txt", naglowek);
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            File.AppendAllText("wyniki_srednie.txt", naglowek);
        }

        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //File.AppendAllText("wyniki_srednie_" + dbg_numer_ewolucji + ".txt", Convert.ToString(dbg_suma / (float)this.liczebnosc_populacji) + Environment.NewLine);
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        File.AppendAllText("wyniki_srednie.txt", Convert.ToString(dbg_suma / (float)this.liczebnosc_populacji) + Environment.NewLine);
        // Debug end //////////////////////////////////////////////////////////////////////////////////////
    }

    private void dbg_dopisz_geny_populacji(List<DaneOsobnika> dane)
    {
        // Debug begin //////////////////////////////////////////////////////////////////////////////////////
        // ZAPIS GENOW NOWEJ POPULACJI DO PLIKU
        File.AppendAllText("atrybuty.txt", "Populacja " + this.p + Environment.NewLine);
        string dbg = string.Empty;
        for (int i = 0; i < this.liczebnosc_populacji; i++)
        {
            for (int j = 0; j < 16; j++)
                dbg += dane[i].geny[j] + ", ";
            dbg += Environment.NewLine;
        }
        File.AppendAllText("atrybuty.txt", dbg + Environment.NewLine);
        // Debug end //////////////////////////////////////////////////////////////////////////////////////
    }

    private void dbg_wypisz_wiersz(string tytul, List<DaneOsobnika> dane)
    {
        // Wypisuje geny dwoch oobnikow
        string debug = string.Empty;
        for (int i = 0; i < 16; i++)
            debug += dane[0].geny[i] + ", ";
        debug += Environment.NewLine;
        for (int i = 0; i < 16; i++)
            debug += dane[1].geny[i] + ", ";
        print(tytul + Environment.NewLine + debug);
    }

    private void dbg_wypisz_float_wiersz(string tytul, List<float> dane)
    {
        string debug = string.Empty;
        for (int i = 0; i < dane.Count; i++)
            debug += dane[i] + ", ";
        debug += Environment.NewLine;
        print(tytul + Environment.NewLine + debug);
    }

    private void dbg_dopisz_geny_do_pliku(string plik, List<DaneOsobnika> dane)
    {
        File.AppendAllText(plik, "Populacja " + this.p + Environment.NewLine);
        string dbg = string.Empty;
        for (int i = 0; i < this.liczebnosc_populacji; i++)
        {
            for (int j = 0; j < 16; j++)
                dbg += dane[i].geny[j] + ", ";
            dbg += Environment.NewLine;
        }
        File.AppendAllText(plik, dbg + Environment.NewLine);
    }

    private void dbg_zapisz_najlepszego_osobnika()
    {
        string s = string.Empty;
        
        s += "Sposrod " + ilosc_populacji + " populacji zlozonych z " + liczebnosc_populacji + " osobnikow kazda" + Environment.NewLine;
        s += "(Co daje laczna liczbe " + ilosc_populacji * liczebnosc_populacji + " osobnikow)" + Environment.NewLine;
        s += "najdalej zaszedl osobnik o nastepujacych genach:" + Environment.NewLine + Environment.NewLine;

        if (najlepszy_osobnik.geny.Count == 16)
        {
            for (int i = 0; i < 16; i++)
                s += najlepszy_osobnik.geny[i] + ", ";

            s += Environment.NewLine + Environment.NewLine + "z wynikiem: " + najlepszy_osobnik.wynik;
        }
        else
            s += "Zaden osobnik nie przekroczyl progu zerowego.";
            
        File.AppendAllText("najlepszy_osobnik.txt", s);
    }
}
