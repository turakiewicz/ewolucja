﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnorujKolizje : MonoBehaviour {

    public GameObject prawaGorna;
    public GameObject prawaDolna;
    public GameObject lewaGorna;
    public GameObject lewaDolna;

	// Use this for initialization
	void Start () {
        BoxCollider2D t = gameObject.GetComponent<BoxCollider2D>();
        BoxCollider2D pg = prawaGorna.GetComponent<BoxCollider2D>();
        BoxCollider2D pd = prawaDolna.GetComponent<BoxCollider2D>();
        BoxCollider2D lg = lewaGorna.GetComponent<BoxCollider2D>();
        BoxCollider2D ld = lewaDolna.GetComponent<BoxCollider2D>();

        Physics2D.IgnoreCollision(t, pd);
        Physics2D.IgnoreCollision(t, ld);
        Physics2D.IgnoreCollision(pd, ld);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
