Wsp�czynniki algorytmu s� hardkodowane, dlatego 'ewo.exe' posiada tylko jeden, niezmienny zestaw. Wszystkie wsp�czynniki mo�na odczyta� z pliku 'wyniki_srednie.txt', kt�ry utworzy si� po uruchomieniu 'ewo.exe' i symulacji kilku pokole�.

Dodatkowo powstan� pliki:

'atrybuty.txt' - zawiera atrybuty wszystkich osobnik�w w ka�dym pokoleniu

'najlepszy_osobnik' - zawiera atrybuty osobnika, kt�ry przeszed� najdalej ze wszystkich symulowanych osobnik�w (nie jest to osobnik "uniwersalnie najlepszy", poniewa� silnik fizyczny Unity zawsze prowadzi przybli�one obliczenia, wi�c ka�da symulacja jest inna - je�li przeprowadzimy symulacj� z osobnikiem, kt�ry przeszed� X, to wcale nie oznacza, �e osobnik z takimi samymi atrybutami przejdzie X w nast�pnej symulacji)

'wyniki.txt' - zawiera wyniki poszczeg�lnych osobnik�w we wszystkich pokoleniach