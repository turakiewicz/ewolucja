## About the project
It's basically a genetic algorythm that teaches 2-legged 2D individuals how to move. I needed to use physics in this so I chose the Unity Engine, because I've had some experience with it and just because it's such a nice engine.
Scripts are written in C#. All comments and names are in Polish.

## Main idea
All individuals look like this (only coloured):

![Ops](https://bytebucket.org/turakiewicz/ewolucja/raw/9ebe495f64e29230e846b5b1dbc7183ed985c8ef/readme_files/rm0.PNG)

They have rectangular body and two, two-element legs. All leg elements have four attributes:

* _initial angle_
* _angular velocity_
* _lower angle limit_
* _upper angle limit_

So the element is created with _initial angle_ and than it rotates with _angular velocity_ from _lower angle_ to _upper angle_ back and forth.
The individual can be described with 16 floating point numbers (can call them genes), 4 for every leg element.

The idea is to create a population of creatures with random attributes, check how far did they manage to walk and create the next population with the best individuals from the population before, changing them a bit. Again and again.

## The algorithm
1. First, as said, the random population is made.
2. After lifetime of the population we draw which individuals will survive. For every individual of the new population we choose from last one with some probability. Of course, the further the creature went, the greater its choosing probability is.
3. Later there is a crossover. Individuals are mixed up with other individuals. The genotype of two creatures is divided in two parts, the second parts are then replaced.
4. Genes are mutated with certain probability and random (but limited) value.
5. A creature with all random genes is placed instead of last one in the list (to make sure we find globally best solution not only locally).
6. That's all, we get back to the second point untill we reach the choosen number of populations.

## Does it really work?
At the beginning we have a pile of creatures in the middle:

![Ops](https://bytebucket.org/turakiewicz/ewolucja/raw/23f63fb7533cd79165ff6098feaa4d69cd283c9e/readme_files/rm1.PNG)

Then all populations go right as we expected:

![Ops](https://bytebucket.org/turakiewicz/ewolucja/raw/23f63fb7533cd79165ff6098feaa4d69cd283c9e/readme_files/rm3.PNG)

And here we have some statistics:

![Ops](https://bytebucket.org/turakiewicz/ewolucja/raw/9ebe495f64e29230e846b5b1dbc7183ed985c8ef/readme_files/rm4.PNG)